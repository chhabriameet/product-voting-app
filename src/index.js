import ReactDOM from 'react-dom';

import './index.css';
// import App from './App';
import ProductList from './components/ProductList';

ReactDOM.render(<ProductList />, document.getElementById('content'));
