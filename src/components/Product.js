import React from "react";

class Product extends React.Component {
    // constructor(props) {
    //     super(props);
    //     this.handleUpVote = this.handleUpVote.bind(this);
    // }
    handleUpVote = () => {
        this.props.onUpVote(this.props.id);
    }
    render() {
        return (
            <div className="item">
                <div className="image">
                    <img src={this.props.productImageUrl}/>
                </div>
                <div className="middle aligned content">
                    <div className="header">
                        <a onClick={this.handleUpVote}>
                            <i className="large caret up icon" />
                        </a>
                        {this.props.votes}
                    </div>
                    <div className="description">
                        <a>{this.props.title}</a>
                        <p>{this.props.description}</p>
                    </div>
                    <div className="extra">
                        <span>Submitted By:</span>
                        <img 
                            className="ui avatar image"
                            src={this.props.authorImageUrl}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default Product;